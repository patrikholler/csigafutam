import java.util.Scanner;

public static void main(String[] args) {
    String[] colors = {"piros", "zöld", "kék"};
    int roundsNumber = 5;

    System.out.println("Indul a csiga verseny!");
    System.out.println("Fogadj, hogy melyik csiga fog nyerni a piros a zold vagy a kek:");
    Scanner scanner = new Scanner(System.in);
    String betOn = scanner.nextLine();
    System.out.println();
    Race race = new Race(colors, betOn, roundsNumber);
    race.runRace();

    scanner.close();
}