import java.util.Random;

public class Race {
    private Snail[] snails;
    private Snail winner;
    private String betOn;
    private int roundsNumber;

    public Race(String[] colors, String betOn, int roundsNumber) {
        snails = new Snail[colors.length];
        for (int i = 0; i < colors.length; i++) {
            snails[i] = new Snail(colors[i]);
        }
        this.betOn = betOn;
        this.roundsNumber = roundsNumber;
    }

    public void runRace() {
        Random rand = new Random();

        for (int round = 1; round <= roundsNumber; round++) {
            System.out.println(round + ". kör");
            for (Snail snail : snails) {
                if (rand.nextDouble() <= 0.2) { // 20% esély gyorsításra
                    snail.move(true);
                    System.out.println("A " + snail.getColor() + " csiga gyorsítót kapott, így " + snail.getPosition() + " egységnyit ment.");
                } else {
                    snail.move(false);
                    System.out.println("A "+snail.getColor() + " csiga " + snail.getPosition() + " egységnyit ment.");
                }
            }
            System.out.println();
        }

        winner = snails[0];
        for (Snail snail : snails) {
            if (snail.getPosition() > winner.getPosition()) {
                winner = snail;
            }
        }

        System.out.println("=== A versenyt a " + winner.getColor()+ " csiga nyerte ===");

        if (winner.getColor().equals(betOn)) {
            System.out.println("=== Gratulálok, nyertél! ===");
        } else {
            System.out.println("=== Sajnos rosszul tippeltél. ===");
        }
    }
}