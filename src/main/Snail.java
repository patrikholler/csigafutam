import java.util.Random;

class Snail {
    private String color;
    private int position;

    public Snail(String color) {
        this.color = color;
        this.position = 0;
    }

    public String getColor() {
        return color;
    }

    public int getPosition() {
        return position;
    }

    public void move(boolean accelerate) {
        Random rand = new Random();
        int distance = rand.nextInt(4); // Random 0-3 közötti érték
        if (accelerate) {
            distance *= 2;
        }
        position += distance;
    }
}